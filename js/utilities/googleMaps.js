// protecting scope of inner vars
(function(){

  var _GoogleMapsLoadStatus = 'unloaded';

  /* A function to asynchronously load google maps.  Will call the provided callback function once maps has loaded.
   This function protects against loading the library multiple times, while gracefully handling callbacks as expected.
   Parameters:
   callback: the method to be called once the map library has been asynchronously loaded.
   */
  function loadGoogleMaps(callback){
    var gmapsApiKey = 'AIzaSyAwaCtIRw6dWIPzChSgooLmb1WjeP_pY_g';

    if(_GoogleMapsLoadStatus == 'loaded'){
      callback ? callback() : alert('Google Maps Loaded Successfully!  Please supply a callback function when using loadGoogleMaps()');
      return;
    }
    else if(_GoogleMapsLoadStatus == 'loading'){
      var statusChecker = setInterval(function(){
        // every 100ms, check to see if the library has finished loading
        if(_GoogleMapsLoadStatus == 'loaded') {
          callback ? callback() : alert('Google Maps Loaded Successfully!  Please supply a callback function when using loadGoogleMaps()');
          clearInterval(statusChecker);
        }
      }, 100);
      return;
    }
    // Maps has not been loaded and should be loaded asynchronously
    // intentionally global for google maps callback
    window.googleMapLoadedCallback = function () {
      _GoogleMapsLoadStatus = 'loaded';

      callback ? callback() : alert('Google Maps Loaded Successfully!  Please supply a callback function when using loadGoogleMaps()');
    };

    if(typeof google === 'object' && typeof google.maps === 'object') {
      // map script is already loaded, jump straight to callback.
      window.googleMapLoadedCallback();
    }
    else {
      // load google maps asynchronously, call mapLoadedCallback when done.
      _GoogleMapsLoadStatus = 'loading';
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = 'https://maps.googleapis.com/maps/api/js?libraries=places&key=' + gmapsApiKey + '&sensor=false&callback=window.googleMapLoadedCallback';
      document.body.appendChild(script);
    }
  }

  /* A function to instantiate a google map.
   Parameters:
   domElement: the containing DOM element for the google map.
   mapOptions (optional): The mapOptions object used in instantiating the map.  Default minimal values will be used if not supplied.
       see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
   */
  function getGoogleMap(domElement, mapOptions){
    if(!mapOptions){
      mapOptions = {
        zoom: 13,
        center: new google.maps.LatLng(-34.397, 150.644)
      }
    }
    return new google.maps.Map(domElement, mapOptions);
  }

  /* A function to instantiate a google places service object.  Will use the supplied map or generate one of not supplied.
   Parameters:
   googleMap (optional): a map object which will be used in the creation of the places service.  If not supplied, one will be created for this purpose.
   */
  function getGooglePlacesService(googleMap){
    if(googleMap)
      return new google.maps.places.PlacesService(map);

    return new google.maps.places.PlacesService(getGoogleMap(document.createElement("DIV")));
  }

  /* A function to instantiate a new google maps geocoder object.
   */
  function getGoogleGeocoder(){
    return new google.maps.Geocoder();
  }

  /* A function to instantiate a google map marker without an initial position.
   Parameters:
   googleMap: a map object which will be used in the creation of the places service.
   */
  function getGoogleMapMarker(googleMap){
    return new google.maps.Marker({ map: googleMap, position: null })
  }

  // declare the globally available functions
  window.loadGoogleMaps = loadGoogleMaps;
  window.getGoogleMap = getGoogleMap;
  window.getGooglePlacesService = getGooglePlacesService;
  window.getGoogleGeocoder = getGoogleGeocoder;
  window.getGoogleMapMarker = getGoogleMapMarker;
})();