/* A function to extract json elements safely from an unpredictable JSON structure.
Parameters:
    json: the incoming json payload to parse (must be decoded into javascript object).
    selector:  the selector string in the form of 'PrimaryAddress.StreetAddressLine[0].LineText' or 'PrimaryAddress.StreetAddressLine.0.LineText'
    defaultVal: the default value to return if the element doesn't exist.  Blank by default.
    strictArrays:  If false, will assume element [0] when parsing the json and an index for an array wasn't specified.
        Additionally will remove indexes if provided for non-array elements in the json tree.  Default to false.
*/
function getJsonElement(json, selector, defaultVal, strictArrays){
    defaultVal = typeof defaultVal !== 'undefined' ? defaultVal : null; // default
    strictArrays = typeof strictArrays !== 'undefined'? strictArrays : false; // default

    if(selector.length==0 || typeof json === 'undefined') { // base case
        return json ? json : defaultVal;
    }

    var cSelector; // current selector
    var newSelector;
    var pIndex = selector.indexOf('.');
    if(pIndex != -1){
        cSelector = selector.substring(0, pIndex);
        newSelector = selector.substring(pIndex+1, selector.length);

    } else {
        cSelector = selector;
        newSelector = '';
    }

    // Remove any bracketed indexes such as data[0] and tack them onto the next selector string.
    var arrayIndex = '';
    var bracketIndex = cSelector.indexOf('[');
    if(bracketIndex != -1){
        var rBracketIndex = cSelector.indexOf(']');
        if(rBracketIndex == -1)
            return defaultVal;

        arrayIndex = cSelector.substring(bracketIndex+1, rBracketIndex);
        cSelector = cSelector.substring(0, bracketIndex);
    }

    if(arrayIndex)
        newSelector = arrayIndex + '.' + newSelector;

    // fix arrays if not in strict mode
    if(!strictArrays){
        // If the json is an array and the selector isn't an index, select index 0, push current selector to next iteration.
        if((json instanceof Array) && isNaN(cSelector)){
            newSelector = selector;
            cSelector = 0;
        }
        // If the json isn't an array and the selector is an index, try string literal and drop index if nonexistent.
        else if (!(json instanceof Array) && !isNaN(cSelector)){
            // Try selection by field literal (fields names can be numbers)
            if(typeof json[cSelector] === 'undefined'){
                // If invalid, discard index and move into next selector.
                return getJsonElement(json, newSelector, defaultVal, strictArrays)
            }
        }
    }

    try{
        return getJsonElement(json[cSelector], newSelector, defaultVal, strictArrays);
    } catch(err){
        return defaultVal;
    }
}

/* A function to strip HTML tags and return the raw text.
 */
function stripHtml(html)
{
   var tmp = document.createElement("DIV");
   tmp.innerHTML = html;
   return tmp.textContent || tmp.innerText || "";
}

/* A function to flatten a javascript object, likely constructed from json returned by Direct services.
Will return an array of elements containing a name, a full name, a friendly name, and indentation level, and a value.
 */
function flatten(obj)
{
    var _transformName = function(arr)
    {
        var nm = arr[arr.length-1];
        if (nm=='$')
            nm = arr[arr.length-2];
        if (nm[0]=='@')
            nm = arr[arr.length-2] + ' ' + nm.substr(1);
        var nm2 = '';
        for (var i=0; i<nm.length; i++)
        {
            if (i>0 && nm[i].toUpperCase()==nm[i])
                nm2 += ' ';
            nm2 += nm[i];
        }
        nm2 = nm2.replace(/D N B/g, 'D&B');
        return nm2;
    }
    var _flatten = function(o, prefix)
    {
        if (typeof(prefix)=='undefined')
        {
            prefix = [];
        }
        var arr = [];
        for (var k in o)
        {
            if (!o.hasOwnProperty(k))
                continue;
            var nameParts = prefix.concat(k);
            if (typeof(o[k]) == 'object') {
                arr.push({
                    name: k,
                    fullName: nameParts,
                    friendlyName: _transformName(nameParts),
                    indent: nameParts.length-1,
                    value: null
                });
//                var addl = _flatten(o[k], nameParts);
                arr = arr.concat(_flatten(o[k], nameParts));
            }
            else
                arr.push({
                    name: k,
                    fullName: nameParts,
                    friendlyName: _transformName(nameParts),
                    indent: nameParts.length-1,
                    value: o[k]});
        }
        return arr;
    }

    var arr = _flatten(obj);
//    arr.sort(function(a,b) { return a.friendlyName==b.friendlyName?0:a.friendlyName<b.friendlyName?-1:1 })
    return arr;
}

/* A function to normalize scores into a float of range [0, 1].
 */
function normalizeScore(score, min, max, reversed)
{
    if(score == '-')
        return 1;

    if(score < min) score = min; //fix for customer file with bad scores.
    if(score > max) score = max; //fix for customer file with bad scores.

    if (!isNaN(score)){ //Number
        // score is 1-9
        // 1->100%
        // 9->0%
        if(reversed)
            score = max - score;
    } else { //String CUSTOM solution for VIAB Rating
        // A-G and H-M
        if (score>='A' && score<='G') {
            score = score.charCodeAt(0)
            min = 'A'.charCodeAt(0); // -> 100
            max = 'G'.charCodeAt(0); // -> 0
            score = max - score;
        } else if (score<='Z') {
            score = score.charCodeAt(0)
            min = 'A'.charCodeAt(0); // -> 100
            max = 'Z'.charCodeAt(0); // -> 0
            score = max - score;
        }
    }
    var range = max - min;
    score = score / range;
    return score * 100; // convert to a 0 - 100 (percentage) scale
}

/* Normalizes the viability score and assigns a percentage value for the gradients.
 */
function viabScoreToPct(data){
    return normalizeScore(data, 1, 9, true);
}

/* Normalizes the fss score and assigns a percentage value for the gradients.
 */
function fssScoreToPct(data){
    return normalizeScore(data, 0, 5, true);
}

/* Normalizes the ccs score and assigns a percentage value for the gradients.
 */
function ccsScoreToPct(data){
    return fssScoreToPct(data);
}

/* Normalizes the ser score and assigns a percentage value for the gradients.
 */
function serScoreToPct(data){
    return viabScoreToPct(data);
}

/* Normalizes the paydex score and assigns a percentage value for the gradients.
 */
function paydexScoreToPct(data){
    return normalizeScore(data, 0, 100, false);
}

/* A function to convert a normalized score into a color value
 */
function percentageToColorVal(num) {
    var colors = ['#b54039', '#ffc341', '#30a567'];
    var colorNum = (num/100) * (colors.length-1);
    var diffBetweenColors = colorNum - Math.floor(colorNum);
    var num1 = Math.floor(colorNum);
    var num2 = Math.ceil(colorNum);
    var color1 = colors[num1];
    var color2 = colors[num2];
    var color = blendColor(color1, color2, diffBetweenColors);
    return color;
}

/* A function to mathematically blend 2 colors based upon the blend value, where 0 <= blend <= 1
 */
function blendColor(hexColor1, hexColor2, blend)
{
    if (typeof(blend)==='undefined')
        blend = 0.5;
    var c1 = hexToRgb(hexColor1);
    var c2 = hexToRgb(hexColor2);
    var color = [];
    for(var i=0; i<3; i++)
    {
        var delta = c2[i] - c1[i];
        var c = c1[i] + blend * delta;
        color[i] = c;
    }
    return rgbToHex(color);
}

/* Helper function to convert RGB colors to hex values
 */
function rgbToHex(rgb) {
    var componentToHex = function(c) {
        var hex = Math.round(c).toString(16);

        return hex.length == 1 ? "0" + hex : hex;
    };
    return "#" + componentToHex(rgb[0]) + componentToHex(rgb[1]) + componentToHex(rgb[2]);
}

/* Helper function to convert hex values to RGB color values
 */
function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [
        parseInt(result[1], 16),
        parseInt(result[2], 16),
        parseInt(result[3], 16)
    ] : null;
}

/* Converts the unformatted revenue to a formatted value.  Rounds to 2 decimal points, converts to millions/billions..
 */
function formatRevenue(revenue, units, currency){
    if(!revenue || revenue.length == 0)
        return 'unknown';

    if(!currency)
      currency = 'USD';

    var unitText = '';

    if(units === 'SingleUnits'){
        if(revenue > 100000){
            revenue = revenue/1000000;
            units = "Million"
        }
    }

    if(units === "Million"){
        if(revenue > 1000){
            revenue = revenue/1000;
            unitText = "Bil";
        } else {
            unitText = "Mil";
        }
    }

    var formatted = '';
    switch(currency) {
      case 'USD':
        formatted = '$';
        break;
      default:
        formatted = '?';
        console.log('NOT IMPLEMENTED: formateRevenue function does not currently support ' + currency);
    }

    formatted += Math.round(revenue*100)/100; // limit to 2 decimal places
    if(unitText) formatted += (' ' + unitText);
    return formatted;
}

/* This function will trim the string so the result string has no space.
 */
function trim(string){
    var TRIM_RE = /^\s+|\s+$/g;
    return string.replace(TRIM_RE, '');
}

/* This function will parse the url query parameters and return the name,value pair in an array.
 */
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

/* This function will format the phone number and return a string like (xxx) xxx-xxxx
 */
function phoneFormat(phone) {
    if (phone !=undefined) {
        //remove leading 0
        phone = phone.replace(/^0+/, ''); //  -- THIS is questionable behavior, do we actually want this?
        phone = phone.replace(/[^0-9]/g, '');
        phone = phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    }
    return phone;
}

/* This function will format duns number and return a string like xx-xxx-xxxx
 */
function dunsFormat(duns) {
    duns = duns.replace(/[^0-9]/g, '');
    duns = duns.replace(/(\d{2})(\d{3})(\d{4})/, "$1-$2-$3");
    return duns;
}

/* This function will capitalize the first letter of the string
 */
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/* Returns a comma formatted number, ex:  1,234,567.32
 */
function commaFormatNumber(num) {
    try {
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } catch(TypeError) {
        return num;
    }
}

/* Returns a human readable size, ex: 3.0 mB
Parameters:
    si (optional): if true, will work on a conversion factor of 1000.  If false, will work on conversion factor of 1024
 */
function humanFileSize(bytes, si) {
    var thresh = si ? 1000 : 1024;
    if(Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }
    var units = si
        ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
        : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while(Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1)+' '+units[u];
}

/* standardize new line ending to \n
 */
function canonicalizeNewlines(str) {
    return str.replace(/(\r\n|\r|\n)/g, '\n');
}

/* Detects if the user is on a mobile browser, using the user-agent.  Returns true/false.
Parameters:
    includeTablets (optional): if true, will also consider tablets as mobile.
 */
function isMobile(includeTablets) {
  var check = false;
  if (includeTablets)
    (function (a) {
      if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))check = true
    })(navigator.userAgent || navigator.vendor || window.opera);
  else
    (function (a) {
      if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))check = true
    })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
}

/*Function that validates email address through a regular expression.*/
function validateEmail(sEmail) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    return filter.test(sEmail);
}



/*Get the English day of the week
* input: theDate: yyyy-mm-dd e.g. '2011-04-11' */
function getEnglishDayOfWeek(theDate) {
    var weekdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    return theDate ? weekdays[(new Date(theDate)).getDay()] : undefined;
}


/*Get the English month from a date
* input: theDate: yyyy-mm-dd e.g. '2011-04-11' */
function getEnglishMonth(theDate, fullName) {
    var months = fullName ? ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'] :
                            ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    return theDate ? months[(new Date(theDate)).getMonth()] : undefined;
}

/*Get the Full English month from a date
* input: theDate: yyyy-mm-dd e.g. '2011-04-11' */
function getFullEnglishMonth(theDate) {
    return getEnglishMonth(theDate, true);
}
