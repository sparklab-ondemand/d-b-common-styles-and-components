/**
 * Created by shane on 7/27/15.
 */

var Modal = React.createClass({displayName: "Modal",
  /**
   * A react component wrapping the bootstrap modal.
   * === PROPS ===
   * title: the title/header text of the modal
   * subtitle: (optional) the subtitle text for the modal.  Will appear as <small> text in the tiel
   * type: (optional) the type of modal, one of: info, confirm, save.  Default: info
   * show: (optional) will hide/show the modal, as controlled by prop.  Default: true
   * className: (optional) Any additional class names
   * onSave: (optional) callback function which will be trigger when 'save' is pressed on modals of type 'save'.  Can optionally return false which will prevent the modal from hiding.
   * onConfirm: (optional) callback function which will be trigger when 'confirm' is pressed on modals of type 'confirm'
   * onDismiss: (optional) callback function which will be trigger when the modal is dismissed for any reason (including after save/confirm)
   * backdropDismiss: (optional) determines whether clicking outside the modal will dismiss it.  Default: true
   * keyboardDismiss: (optional) determines whether clicking outside the modal will dismiss it.  Default: true
   * preventDismiss: (optional) prevents the user from dismissing the modal.  If using this, you should disable backdropDismiss and keyboardDismiss!  Default: false.
   * saveBtnLabel: (optional) text to display on the save button.  Default: 'save'
   * children: the contents of the modal
   **/
  propTypes: {
    title: React.PropTypes.string.isRequired,
    subtitle: React.PropTypes.string,
    type: React.PropTypes.oneOf([
      'info',
      'confirm',
      'save'
    ]),
    size: React.PropTypes.oneOf(['normal', 'small', 'large']),
    className: React.PropTypes.string,
    show: React.PropTypes.bool,
    onSave: React.PropTypes.func,
    onConfirm: React.PropTypes.func,
    onDismiss: React.PropTypes.func,
    backdropDismiss: React.PropTypes.bool
  },

  getDefaultProps: function () {
    return {
      type: 'info',
      size: 'normal',
      show: true,
      backdropDismiss: true,
      keyboardDismiss: true,
      preventDismiss: false,
      saveBtnLabel: 'Save'
    };
  },

  onSave: function(){
    if(this.props.onSave)
      if(this.props.onSave() == false)  // the onSave parent method may return false to prevent automatic dismissal.
        return;

    $(this.getDOMNode()).modal('hide');
  },

  onConfirm: function(){
    if(this.props.onConfirm)
      this.props.onConfirm();
  },

  onDismiss: function(){
    if(this.props.onDismiss)
      this.props.onDismiss();
  },

  dismissModal: function(){
    // use this function to dismiss a shown modal, this will in turn call onDismiss.
    $(this.getDOMNode()).modal('hide');
  },

  componentDidMount: function () {
    var $modal = $(this.getDOMNode());

    var modalOps = { show: this.props.show };
    if(this.props.preventDismiss || !this.props.backdropDismiss)
      modalOps.backdrop = 'static';  // cannot set these after modal creation

    if(this.props.preventDismiss || !this.props.keyboardDismiss)
      modalOps.keyboard = false;

    $modal.modal(modalOps);

    $modal.on('hidden.bs.modal', function() {
      this.onDismiss();
    }.bind(this));
  },

  componentDidUpdate: function(prevProps, prevState) {
    if(!prevProps.show && this.props.show)
      $(this.getDOMNode()).modal('show');

    if(prevProps.show && !this.props.show)
      $(this.getDOMNode()).modal('hide');
  },

  render: function () {
    var dismissText = this.props.type=='info' ? 'Close' : 'Cancel';
    var classes = ['modal fade'];
    if(this.props.className)
      classes.push(this.props.className);
    classes = classes.join(' ');

    var modalClasses = ['modal-dialog'];
    if(this.props.size == 'large') modalClasses.push('modal-lg');
    else if(this.props.size == 'small') modalClasses.push('modal-sm');
    modalClasses = modalClasses.join(' ');

    return (
      React.createElement("div", {className: classes, id: "myModal", tabIndex: "-1", role: "dialog", "aria-labelledby": "myModalLabel"}, 
        React.createElement("div", {className: modalClasses, role: "document"}, 
          React.createElement("div", {className: "modal-content"}, 
            React.createElement("div", {className: "modal-header"}, 
              !this.props.preventDismiss ? (
                React.createElement("button", {type: "button", className: "close", "data-dismiss": "modal", "aria-label": "Close"}, 
                  React.createElement("span", {"aria-hidden": "true"}, "×")
                )
              ):  null, 
              React.createElement("h4", {className: "modal-title", id: "myModalLabel"}, 
                this.props.title, 
                this.props.subtitle ? React.createElement("small", null, " ", this.props.subtitle) : null
              )
            ), 
            React.createElement("div", {className: "modal-body"}, 
              this.props.children
            ), 
            React.createElement("div", {className: "modal-footer"}, 
              this.props.preventDismiss ? null : (
                React.createElement("button", {type: "button", className: "btn btn-default", "data-dismiss": "modal"}, dismissText)
              ), 
            this.props.type=='save' ?
              React.createElement("button", {type: "button", className: "btn btn-primary", onClick: this.onSave}, this.props.saveBtnLabel)
              : null, 
            this.props.type=='confirm' ?
              React.createElement("button", {type: "button", className: "btn btn-primary", "data-dismiss": "modal", onClick: this.onConfirm}, "Ok")
              : null
            )
          )
        )
      )
    );
  }
});