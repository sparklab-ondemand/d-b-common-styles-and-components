var GooglePlacePhotos = (function () {
  var PhotosList = React.createClass({displayName: "PhotosList",
    /** React component encompassing a google map, and mapping a single location only.
     * === REQUIREMENTS ===
     * utilities/googleMaps.js
     * Loader common component
     * === PROPS ===
     * name: The name of the company/org/location.
     * address: The address string to map - this is equivalent to what one would type into the search field on google maps.  May contain concatenated street, state, company name, territory, etc...
     * className: (optional) any additional CSS classes to add to the element
     * maxPhotos (optional): The maximum number of photos to show. Defaults to 4.
     * height (optional): The height of the thumbnails, in px.  Default: 200
     * width (optional): The width used to request thumbnail images from google, in px.  Default: 500
     **/
    propTypes: {
      name: React.PropTypes.string.isRequired,
      address: React.PropTypes.string.isRequired,
      maxPhotos: React.PropTypes.oneOfType([
        React.PropTypes.number,
        React.PropTypes.string
      ]),
      width: React.PropTypes.oneOfType([
        React.PropTypes.number,
        React.PropTypes.string
      ]),
      height: React.PropTypes.oneOfType([
        React.PropTypes.number,
        React.PropTypes.string
      ])
    },

    getInitialState: function(){
      return {
        placesService: null,
        photosLoaded: false,
        photos: [],
        zoomedPhoto: null
      }
    },

    getDefaultProps: function(){
      return {
        maxPhotos: 4,
        width: 500,
        height: 200,
        photoPosition: 'center'
      }
    },

    componentDidMount: function(){
      // intentionally global for google maps callback
      loadGoogleMaps(function(){
        var placesService = getGooglePlacesService();
        this.setState({placesService: placesService});
      }.bind(this));
    },

    loadPhotos: function() {
      var placeService = this.state.placesService;
      placeService.textSearch({query: this.props.name + ' ' + this.props.address}, function(results, status){
        if(status == google.maps.places.PlacesServiceStatus.OK){
          placeService.getDetails({'reference': results[0].reference}, function(results, status){
            var photos = [];
            if(results.photos) {
              $.each(results.photos, function(i, value) {
                if(i >= this.props.maxPhotos)
                  return false;
                photos.push({
                  url: value.getUrl({maxWidth: Math.floor(window.innerWidth*.9), maxHeight: Math.floor(window.innerHeight*.9)}),
                  thumbnail: value.getUrl({maxWidth: this.props.width})
                });
              }.bind(this));
            }
            this.setState({photos: photos});
          }.bind(this));
        }
        this.setState({photosLoaded: true});
      }.bind(this));
    },

    viewPhoto: function(imgUrl){
      this.setState({zoomedPhoto: imgUrl});
    },

    hidePhoto: function(){
      this.setState({zoomedPhoto: null});
    },

    componentDidUpdate: function(prevProps, prevState){
      if(this.state.placesService && (!this.state.photosLoaded || prevProps.address != this.props.address || prevProps.name != this.props.name))
        this.loadPhotos();
    },

    onKeyDown: function(event){
      if(event.key == 'Escape')
        this.hidePhoto();
    },

    render: function(){
      var classes = ['dnb-google-place-photos'];
      if(this.props.className) classes.push(this.props.className);
      classes = classes.join(' ');

      var photos = this.state.photos.map(function(val, i){
        var photoStyle = {
          minHeight: this.props.height + 'px',
          background: 'url("' + val.thumbnail + '") no-repeat ' + this.props.photoPosition
        };
        return React.createElement("div", {className: "photo", style: photoStyle, key: val.thumbnail, onClick: this.viewPhoto.bind(null, val.url)})
      }.bind(this));

      if(!photos.length)
        return null;

      return (
        React.createElement("div", {className: classes}, 
          this.state.zoomedPhoto ? React.createElement(PhotoView, {src: this.state.zoomedPhoto, dismiss: this.hidePhoto}) : null, 
          React.createElement("div", {className: "row"}, 
            photos
          ), 
          React.createElement("div", {className: "disclaimer"}, "Images provided by Google Maps")
        )
      );
    }
  });

  var PhotoView = React.createClass({displayName: "PhotoView",
    getInitialState: function(){
      return {
        loading: true,
        error: false
      };
    },

    componentDidMount: function(){
      if(this.refs.input) this.refs.input.getDOMNode().focus();

      var image = new Image();
      image.onload = function(){
        this.setState({error: false, loading:false});
      }.bind(this);
      image.onerror = function(){
        this.setState({error: true, loading:false});
      }.bind(this);
      image.src = this.props.src;
    },

    render: function(){
      var styleOverride = this.state.loading ? {backgroundColor: 'transparent'} : {};
      // only run this check if the isMobile function is defined.  (See global utilities.js script).
      var keyboardEsc = window.isMobile ? (isMobile(true) ? false: true) : true;
      return (
        React.createElement("div", {ref: "photoView", className: "view-photo", onClick: this.props.dismiss, style: styleOverride}, 
         keyboardEsc ?
          React.createElement("input", {ref: "input", type: "text", onKeyDown: this.props.dismiss, style: { position: 'fixed', top: '-1000px'}})
          : null, 
          React.createElement("div", {onClick: this.props.dismiss}, 
          this.state.loading ? React.createElement(Loader, null) :
            this.state.error ? React.createElement("div", {className: "error"}, React.createElement("i", {className: "fa fa-exclamation-triangle"}), " Unable load image") : React.createElement("img", {src: this.props.src})
          
          )
        )
      )
    }
  });

  return PhotosList
}).call(this);