var InlineBarGraph = React.createClass({displayName: 'InlineBarGraph',
  /** React component for displaying a simple inline graph.
   * DEPENDENCY:
   * === PROPS ===
   * data: the data to render - should be a simple array of numbers.
   * barColor (optional): the color to use for bars in the bar graph.
   * barWidth (optional): the width of the bars in pixels.  default: 4
   **/
  propTypes: {
    data: React.PropTypes.array.isRequired,
    barColor: React.PropTypes.string,
    width: React.PropTypes.number
  },

  getDefaultProps: function(){
    return {
      type: 'bar',
      barColor: '#13709f',
      barWidth: 4
    }
  },

  componentDidMount: function(){
    this.renderGraph();
  },

  componentDidUpdate: function(prevProps, prevStates){
    this.renderGraph();
  },

  renderGraph: function(){
    $(this.getDOMNode()).sparkline(this.props.data, {type: 'bar', barColor: this.props.barColor, barWidth:this.props.barWidth});
  },

  render: function(){
    return React.createElement("span", {className: "inline-chart"});
  }
});