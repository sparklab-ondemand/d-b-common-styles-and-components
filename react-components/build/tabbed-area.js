var TabbedArea = React.createClass({displayName: 'TabbedArea',
  /** React component representing a tabbed area.
   * === PROPS ===
   * default: the default active tab index (0-indexed).
   * (children): one of more <TabPane /> children.
   **/
  propTypes: {
    default: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ]),
    children: React.PropTypes.oneOfType([
      React.PropTypes.element,
      React.PropTypes.arrayOf(React.PropTypes.element)
    ])
  },

  getInitialState: function(){
    return {
      'activeTab': (this.props.default) ? parseInt(this.props.default) : 0
    }
  },

  clickTab: function(tabIndex, callback){
    this.setState({'activeTab': tabIndex});
    if(callback)
      callback();
  },

  render: function(){
    var children = this.props.children.length != undefined ? this.props.children : [this.props.children];
    var tabs = children.map(function(val, i){
      if(!val) return null;
      return (
        React.createElement("li", {key: i, className: this.state.activeTab == i ? 'active' : ''}, 
          React.createElement("a", {href: '#' + val.props.id, onClick: this.clickTab.bind(null, i, val.props.onSelectTab)}, val.props.tab)
        )
      );
    }, this);
    return (
      React.createElement("div", {role: "tabpanel"}, 
        React.createElement("ul", {className: "nav nav-tabs", role: "tablist"}, 
          tabs
        ), 

        React.createElement("div", {className: "tab-content"}, 
          children[this.state.activeTab]
        )

      )
    );
  }
});


var TabPane = React.createClass({displayName: 'TabPane',
  /** React component representing a standard basic widget.
   * === PROPS ===
   * tab: the friendly text or element to go on the tab for this panel
   * onSelectTab (optional): a callback function executed when this tab is selected.
   * id (optional): the id of this tab panel.
   * className (optional): any classes to assign to this tab.
   * (children): used as the content
   **/
  propTypes: {
    id: React.PropTypes.string,
    className: React.PropTypes.string,
    tab: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.element
    ]).isRequired,
    onSelectTab: React.PropTypes.func
  },

  render: function(){
    return (
      React.createElement("div", {id: this.props.id, className: this.props.className}, this.props.children)
    );
  }
});