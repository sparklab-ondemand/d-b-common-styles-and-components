var RiskGradientBar = React.createClass({displayName: 'RiskGradientBar',
  /** React component for a risk gradient bar.
   * === PROPS ===
   * label: the character to place on the badge
   * percentage: the left offset in percent (0 - 100)
   **/
  propTypes: {
    label: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ]),
    percentage: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ])
  },

  render: function(){
    return (
      React.createElement("div", {className: "dnb-risk-gradient"}, 
        React.createElement("div", {className: "dnb-risk-gradient-inner"}), 
        React.createElement("div", {className: "dnb-risk-gradient-badge-container", style: {left: this.props.percentage+'%'}}, 
          React.createElement("div", {className: "dnb-risk-gradient-badge"}, 
            React.createElement("div", {className: "dnb-risk-gradient-badge-label"}, this.props.label)
          )
        )
      )
    );
  }
});