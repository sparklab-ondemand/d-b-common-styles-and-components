GlobalHeader = React.createClass({
  /** React global header component, to be used on all apps.
   * === PROPS ===
   * title (optional): the title to display on the header
   * children (optional): any GlobalHeaderItems or GlobalHeaderDropdowns to include on the header
   **/
  propTypes: {
    title: React.PropTypes.string,
    children: React.PropTypes.oneOfType([
      React.PropTypes.arrayOf(React.PropTypes.element),
      React.PropTypes.element
    ]),
    fixed: React.PropTypes.any
  },

  getDefaultProps: function(){
    return {
      search: false
    }
  },

  render: function(){
    var classes = ['navbar navbar-default dnb-global-header'];
    if(this.props.fixed) classes.push('fixed');
    classes = classes.join(' ');

    var header = (
      <nav className={classes}>
        <div className="container-fluid">
          <div className="navbar-header">
          { this.props.children ? (
            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            ): null}

            <a className={'navbar-brand' + (this.props.search ? '' : ' no-search')} href="#">{this.props.title}</a>
          {this.props.search ? (
            <form className="navbar-form navbar-left" role="search" action="javascript:void(0)">
              {this.props.search}
            </form>
          ) : null}
          </div>
        { this.props.children ? (
          <div className="collapse navbar-collapse" id="navbar-collapse">
            <ul className="nav navbar-nav navbar-right">
              {this.props.children}
            </ul>
          </div>
        ) : null}
        </div>
      </nav>
    );

    return this.props.fixed ? (
      <div><div className="dnb-global-header-spacer"/>{header}</div>
    ) : header;
  }
});

GlobalHeaderItem = React.createClass({
  /** React component for a menu item within the global header.
   * === PROPS ===
   * children: the item to display, may be text or an element
   * href (optional): a link for this menu item
   * target (optional): an optional target for the link ex. _blank
   * active (optional): including 'active' in your properties will mark this item as active
   * small (optional): including 'small' in your properties will make this a small square item with left border
   * onClick (optional): a callback function for when this item is clicked
   **/
  propTypes: {
    href: React.PropTypes.string,
    target: React.PropTypes.string,
    active: React.PropTypes.any,
    small: React.PropTypes.any,
    onClick: React.PropTypes.func,
    children: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.element
    ]).isRequired
  },

  getDefaultProps: function(){
    return {
      href: 'javascript: void(0)',
      target: '_self'
    }
  },

  onClick: function(){
    if(this.props.onClick)
      this.props.onClick();
  },

  render: function(){
    var classes = [];
    if(this.props.active) classes.push('active');
    if(this.props.small) classes.push('small-item');
    classes = classes.join(' ');
    return (
      <li className={classes}><a href={this.props.href} target={this.props.target} onClick={this.onClick}>{this.props.children}</a></li>
    );
  }
});


GlobalHeaderDropdown = React.createClass({
  /** React component for a dropdown menu within the global header.
   * === PROPS ===
   * toggle: the item to display, may be text or an element
   * children: the GlobalHeaderItems to display in the dropdown
   * small (optional): including 'small' in your properties will make this a small square item with left border
   **/
  propTypes: {
    small: React.PropTypes.any,
    children: React.PropTypes.oneOfType([
      React.PropTypes.arrayOf(React.PropTypes.element),
      React.PropTypes.element
    ]).isRequired,
    toggle: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.element
    ]).isRequired
  },

  onClick: function(){
    if(this.props.onClick)
      this.props.onClick();
  },

  render: function(){
    var classes = ['dropdown'];
    if(this.props.small) classes.push('small-item');
    classes = classes.join(' ');

    return (
      <li className={classes}>
        <a href="javascript: void(0)" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        {this.props.toggle} <span className="caret"></span></a>
        <ul className="dropdown-menu" role="menu">
          {this.props.children}
        </ul>
      </li>
    );
  }
});
