var BasicTable = React.createClass({
  /**
   * A basic table display a header and body.
   *
   * === PROPS ===
   * headers: an array of column headers (strings)
   * rows: a bi-dimensional array (array of arrays of strings or elements) Ex: [['r1c1', 'r1c2', , <span>r1c3</span>], ['r2c1', 'r2c2', 'r2c3']]
   * id: the DOM ID that will be given to the table
   * className (optional): any additional classes other than "table" to incude on this element.
   */

  propTypes: {
    headers: React.PropTypes.array.isRequired,
    rows: React.PropTypes.array.isRequired,
    className: React.PropTypes.string
  },

  getDefaultProps: function(){
    return {
      headers: [],
      rows: [],
      id: null
    }
  },

  render: function(){
    var headers = this.props.headers.map(function(val, i){
      return <th key={i}>{val}</th>;
    });

    var rows = this.props.rows.map(function(row, i){
      var rowCells = row.map(function(val, j){ return <td key={j}>{val}</td> });
      return <tr key={i}>{rowCells}</tr>
    });

    var classes = ['table table-striped'];
    if(this.props.className)
      classes.push(this.props.className);
    classes = classes.join(' ');

    return (
      <table className={classes} ref="table" id={this.props.id}>
        <thead><tr>{headers}</tr></thead>
        <tbody>{rows}</tbody>
      </table>
    );
  }
});