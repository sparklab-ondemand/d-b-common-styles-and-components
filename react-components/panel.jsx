var Panel = React.createClass({
  /** React component representing a basic panel with optional headers and footers.
   * === PROPS ===
   * id: (optional) the element ID
   * header: (optional) the widget header, may be string or element
   * footer: (optional) the widget footer, may be string or element
   * className: (optional) any additional CSS classes to add to the element
   * type: (optional) the type of panel, effects styling.   Options:  default, primary, success, info, warning, danger
   * (children): used as the content
   **/
  propTypes: {
    id: React.PropTypes.string,
    header: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.element
    ]),
    footer: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.element
    ]),
    className: React.PropTypes.string,
    type: React.PropTypes.oneOf(['default', 'primary', 'success', 'info', 'warning', 'danger']),
  },

  getDefaultProps: function() {
    return {
      type: 'default'
    }
  },

  render: function () {
    var classes = ['panel'];
    classes.push('panel-' + this.props.type);
    if(this.props.className) classes.push(this.props.className);
    classes = classes.join(' ');
    return (
      <div className={classes} id={this.props.id}>
      { this.props.header ? (
        <div className="panel-heading">{this.props.header}</div>
      ): null }

        <div className="panel-body">{this.props.children}</div>

      { this.props.footer ? (
        <div className="panel-footer">{this.props.footer}</div>
      ) : null }
      </div>
    );
  }
});