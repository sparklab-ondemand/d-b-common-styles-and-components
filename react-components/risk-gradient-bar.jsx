var RiskGradientBar = React.createClass({
  /** React component for a risk gradient bar.
   * === PROPS ===
   * label: the character to place on the badge
   * percentage: the left offset in percent (0 - 100)
   **/
  propTypes: {
    label: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ]),
    percentage: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ])
  },

  render: function(){
    return (
      <div className="dnb-risk-gradient">
        <div className="dnb-risk-gradient-inner"></div>
        <div className="dnb-risk-gradient-badge-container" style={{left: this.props.percentage+'%'}}>
          <div className="dnb-risk-gradient-badge">
            <div className="dnb-risk-gradient-badge-label">{this.props.label}</div>
          </div>
        </div>
      </div>
    );
  }
});