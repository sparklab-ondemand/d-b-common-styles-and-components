var ScrollingTable = React.createClass({
  /**
   * This table component resides within a container meant to overflow-y scroll, while keeping the header stuck in place.
   * Dependency: jquery-floatThead/1.2.10/jquery.floatThead.min.js
   *
   * === Example CSS ===
   *   .table-container {
   *     max-height: 500px;
   *     overflow-y: auto;
   *   }
   *
   * === PROPS ===
   * headers: an array of column headers (strings)
   * rows: a bi-dimensional array (array of arrays of strings or elements) Ex: [['r1c1', 'r1c2', <span>r1c3</span>], ['r2c1', 'r2c2', 'r2c3']]
   * id: the DOM ID that will be given to the table
   */

  propTypes: {
    headers: React.PropTypes.array.isRequired,
    rows: React.PropTypes.array.isRequired
  },

  getDefaultProps: function(){
    return {
      headers: [],
      rows: [],
      id: ''
    }
  },

  componentDidMount: function(){
    // initialize the floating thead and save the reference in state.
    this.floatingThead = $(this.refs.table.getDOMNode()).floatThead({
        scrollContainer: function($table){
          return $table.closest('.table-container');
        }
    });
  },

  componentDidUpdate: function(prevProps, prevState){
    this.floatingThead.floatThead('reflow');
  },

  render: function(){
    var headers = this.props.headers.map(function(val, i){
      return <th>{val}</th>;
    });

    var rows = this.props.rows.map(function(row, i){
      var rowCells = row.map(function(val, i){ return <td>{val}</td> });
      return <tr>{rowCells}</tr>
    });

    return (
      <div className="table-container">
        <table className="table" ref="table" id={this.props.id}>
          <thead><tr>{headers}</tr></thead>
          <tbody>{rows}</tbody>
        </table>
      </div>
    );
  }
});