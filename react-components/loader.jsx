/**
 * Created by shane on 7/21/15.
 */

var Loader = React.createClass({
  /**
   * A very simple loader.  Will blank out the widget and display an SVG animation.
   * === PROPS ===
   * size (optional): if provided, will limit the size of the gears, in pixels
   **/
    propTypes: {
      size: React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.number
      ])
    },

  render: function () {
    var style = {};
    if(this.props.size)
      style['backgroundSize'] = this.props.size + 'px';

    return (
      <div className="dnb-loader">
        <div className="spinner" style={style}/>
      </div>
    );
  }
});