var MapSingleLocation = React.createClass({
  /** React component encompassing a google map, and mapping a single location only.
   * === REQUIREMENTS ===
   * This component requires the utilities/googleMaps.js script
   * === PROPS ===
   * address: The address string to map - this is equivalent to what one would type into the search field on google maps.  May contain concatenated street, state, company name, territory, etc...
   * height (optional): The height of the map, in px.  Default: 300
   * zoom (optional): The zoom level of the google map.  Defaults to 13.
   * styles (optional): An optional styles array to change the rendering of the google map.  See: https://developers.google.com/maps/documentation/javascript/styling
   **/
  propTypes: {
    address: React.PropTypes.string,
    zoom: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ]),
    styles: React.PropTypes.arrayOf(React.PropTypes.object),
    height: React.PropTypes.oneOfType([
      React.PropTypes.number,
      React.PropTypes.string
    ])
  },

  getInitialState: function(){
    return {
      map: null,
      geocoder: null,
      marker: null,
      isMapped: false,
      approximate: false
    }
  },

  getDefaultProps: function(){
    return {
      height: 300,
      zoom: 13,
      styles: [
          {
            "featureType": "landscape",
            "stylers": [
              {
                "hue": "#F1FF00"
              },
              {
                "saturation": -27.4
              },
              {
                "lightness": 9.4
              },
              {
                "gamma": 1
              }
            ]
          },
          {
            "featureType": "road.highway",
            "stylers": [
              {
                "hue": "#0099FF"
              },
              {
                "saturation": -20
              },
              {
                "lightness": 36.4
              },
              {
                "gamma": 1
              }
            ]
          },
          {
            "featureType": "road.arterial",
            "stylers": [
              {
                "hue": "#00FF4F"
              },
              {
                "saturation": 0
              },
              {
                "lightness": 0
              },
              {
                "gamma": 1
              }
            ]
          },
          {
            "featureType": "road.local",
            "stylers": [
              {
                "hue": "#FFB300"
              },
              {
                "saturation": -38
              },
              {
                "lightness": 11.2
              },
              {
                "gamma": 1
              }
            ]
          },
          {
            "featureType": "water",
            "stylers": [
              {
                "hue": "#00B6FF"
              },
              {
                "saturation": 4.2
              },
              {
                "lightness": -63.4
              },
              {
                "gamma": 1
              }
            ]
          },
          {
            "featureType": "poi",
            "stylers": [
              {
                "hue": "#9FFF00"
              },
              {
                "saturation": 0
              },
              {
                "lightness": 0
              },
              {
                "gamma": 1
              }
            ]
          }
        ]
    }
  },

  refreshMap: function(){
    // function trigger resizing and re-centering of the map.
    google.maps.event.trigger(this.state.map, "resize");
    if(this.state.marker.position)
      this.state.map.setCenter(this.state.marker.position);
  },

  componentDidMount: function(){
    // intentionally global for google maps callback
    loadGoogleMaps(function(){
      var mapOptions = {
        zoom: parseInt(this.props.zoom),
        center: new google.maps.LatLng(-34.397, 150.644),
        styles: this.props.styles
      };

      var map = getGoogleMap(this.refs.mapContainer.getDOMNode(), mapOptions);
      var geocoder = getGoogleGeocoder();
      var marker = getGoogleMapMarker(map);
      this.setState({map: map, geocoder: geocoder, marker: marker});
      $(window).resize(function(){
        google.maps.event.trigger(map, "resize");
        if(marker.position)
          map.setCenter(marker.position);
      });
    }.bind(this));
  },

  mapCompany: function(address) {
    this.state.map.getStreetView().setVisible(false);
    this.state.geocoder.geocode( {address: address}, function(results, status) {
      var error = false, approximate = false;

      if (status == google.maps.GeocoderStatus.OK) {
        this.state.map.setCenter(results[0].geometry.location);
        this.state.marker.setPosition(results[0].geometry.location);

        approximate = (results[0].geometry.location_type == 'APPROXIMATE');
      }
      else
        error = true;

      this.setState({isMapped: true, approximate: approximate, error: error});
    }.bind(this));
  },

  componentDidUpdate: function(prevProps, prevState){
    if(this.state.map && (!this.state.isMapped || prevProps.address != this.props.address))
      this.mapCompany(this.props.address);
  },

  render: function(){
    return (
      <div className="dnb-map-single-location">
      { this.state.approximate ?
        <div className="map-overlay approximate"><p>Location on map is approximated for this company</p></div>
        : null
        }
      { this.state.error ?
        <div className="map-overlay error"><p>Unable to Map Current Location<br/><small>Please try another search</small></p></div>
        : null
        }
        <div className="dnb-google-map" ref="mapContainer" style={{minHeight: this.props.height + 'px'}}></div>
      </div>
    );
  }
});