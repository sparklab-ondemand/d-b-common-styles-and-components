/* Autocomplete component modified from: https://github.com/prometheusresearch/react-autocomplete
 * Original docs: http://prometheusresearch.github.io/react-autocomplete/index.html# */

var Autocomplete = React.createClass({
  /** React component for general autocomplete usage.
   * === PROPS ===
   * search: The function to perform the search with the following arguments: options, searchTerm, callback, searchType.  The function should perform the search and feed call the callback providing the searchTerm and the list of results.  Each object in the array should contain the id and title properties at the least.
   * resultRenderer (optional): A React Component or Function used to display the search results.  Default: Basic result renderer.
   * onChange: The callback function triggered when a result is selected.  Should take a single parameter, result.  This will be the object from the result array sent to the callback from the performSearch function.
   * showButton (optional): Boolean, whether or not to show the input-group-addon button with icon.  Defaults to false
   * types (optional): An array of search types that will be presented in a dropdown:  Ex. ['Company', 'Person']  The selected option will be passed to the search function.
   * options (optional): Should be an array of objects for cases where the set of choices is known at load (no AJAX call), not likely to be used often.  Opaque object which will be served as an argument to search function, by default search expects an array with of objects of shape {title: ..., id: ...}
   * searchTerm (optional): The default search term, if needed.
   * searchDelay (optional): Time in milliseconds to delay after last keystroke to call the API.  Defaults to 250.
   * minChars (optional): The minimum number of characters required to perform search. Defaults to 2.
   * noResults (optional): The text to display when no results are found. Default: 'No Results'
   **/
  propTypes: {
    search: React.PropTypes.func.isRequired,
    resultRenderer: React.PropTypes.oneOfType([  // the component to render an individual result
      React.PropTypes.element,
      React.PropTypes.func
    ]),
    onChange: React.PropTypes.func.isRequired,
    types: React.PropTypes.arrayOf(React.PropTypes.string),
    options: React.PropTypes.arrayOf(React.PropTypes.object),
    searchTerm: React.PropTypes.string,
    searchDelay: React.PropTypes.oneOfType([  // the delay in milliseconds before searching
      React.PropTypes.number,
      React.PropTypes.string
    ]),
    minChars: React.PropTypes.oneOfType([  // the minimum number of characters before searching
      React.PropTypes.number,
      React.PropTypes.string
    ]),
    noResults: React.PropTypes.string
  },

  render: function() {
    var classes = ['dnb-autocomplete input-group'];
    if(this.props.className)
      classes.push(this.props.className);
    if(this.state.showResults)
      classes.push('results-shown');
    if(this.state.showResultsInProgress)
      classes.push('loading');

    var className = classes.join(' ');
    var style = {
      position: 'relative',
      outline: 'none'
    };
    return (
      <div
        className={className}
        onFocus={this.onFocus}
        onBlur={this.onBlur}
        style={style}>
      { this.props.types ? (
        <div className="input-group-btn">
          <button type="button" className="btn btn-type-select dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{this.state.selectedType} <span className="caret"></span></button>
          <ul className="dropdown-menu" role="menu">
            { this.props.types.map(function(val, i){
                return <li><a href="javascript: void(0)" onClick={this.selectType.bind(null, val)}>{val}</a></li>
              }.bind(this))
            }
          </ul>
        </div>
      ) : null}
        <input
          ref="search"
          className="form-control"
          placeholder={this.props.placeholder}
          style={{width: '100%'}}
          onChange={this.onQueryChange}
          onKeyDown={this.onQueryKeyDown}
          value={this.state.searchTerm}
          />
      { this.props.showButton ? (
        <span className="input-group-btn">
          <button type="submit" className="btn btn-primary btn-search"><i className="icon icon-search"></i><span className="sr-only">Submit</span></button>
        </span>
      ) : null }
      { this.state.showResults ? (
        <Results
          className="dropdown-menu results"
          onSelect={this.onValueChange}
          onFocus={this.onValueFocus}
          results={this.state.results}
          focusedValue={this.state.focusedValue}
          renderer={this.props.resultRenderer}
          showResultsInProgress={this.state.showResultsInProgress}
          noResults={this.props.noResults}
          />
      ) : null }
      </div>
    );
  },

  getDefaultProps: function() {
    /** Establishes a default search function if one has not been provided. **/
    return {
      searchDelay: 250,
      minChars: 2,
      search: function(options, searchTerm, cb) {
        /**
         * Default search function, if not provided in props.
         * Search options using specified search term treating options as an array
         * of candidates.
         *
         * @param {Array.<Object>} options
         * @param {String} searchTerm
         * @param {Callback} cb
         */
        if (!options) {
          return cb(searchTerm, []);
        }

        var regexSearchTerm = new RegExp(searchTerm, 'i');

        var results = [];

        for (var i = 0, len = options.length; i < len; i++) {
          if (regexSearchTerm.exec(options[i].title))
            results.push(options[i]);
        }
        cb(searchTerm, results);
      },
      showButton: false,
      resultRenderer: Result,
      noResults: 'No Results'
    };
  },

  getInitialState: function() {
    return {
      results: [],
      showResults: false,
      showResultsInProgress: false,
      searchTerm: this.props.searchTerm || '',
      focusedValue: null,
      selectedType: this.props.types ? this.props.types[0] : null
    };
  },

  reset: function(){
    this.setState(this.getInitialState());
  },

  componentWillMount: function() {
    this.blurTimer = null;
  },

  selectType: function(type) {
    this.setState({selectedType: type});
  },

  showResults: function(searchTerm) {
    /** This function will set the loading state and perform the search.
     * @param {Search} searchTerm
     **/

    this.setState({ results: [], showResultsInProgress: true });
    this.props.search(
      this.props.options,
      searchTerm.trim(),
      this.onSearchComplete,
      this.state.selectedType
    );
  },

  showAllResults: function() {
    /** Forces showing the results if there is a query of length **/
    if (this.state.searchTerm.length >= this.props.minChars)
      this.showResults(this.state.searchTerm);
  },

  onValueChange: function(value) {
    /** Called when one of the suggestions is selected in any fashion **/
    var state = {
      value: value,
      showResults: false
    };

    if (value) state.searchTerm = value.title;

    this.setState(state);
    this.refs.search.getDOMNode().blur();

    if (this.props.onChange) {
      this.props.onChange(value);
    }
  },

  onSearchComplete: function(searchTerm, results) {
    /** Called when the search has completed and results need to be displayed **/
    // ignore if the search term has changed
    if(searchTerm != this.state.searchTerm) return;

    this.setState({
      showResultsInProgress: false,
      showResults: true,
      results: results
    });
  },

  onValueFocus: function(value) {
    /** Called when the user hover/focuses on one of the suggestions **/
    this.setState({focusedValue: value});
  },

  onQueryChange: function(e) {
    /** Called when the user query has changed **/
    var searchTerm = e.target.value;
    this.setState({
      searchTerm: searchTerm,
      focusedValue: null
    });

    if(searchTerm.length < this.props.minChars) {
      this.setState({
        showResults: false,
        showResultsInProgress: false
      });
      return;
    }

    setTimeout(function(){
      // only perform search if the query hasn't changed since this timer began.
      if(searchTerm == this.state.searchTerm)
        this.showResults(searchTerm);
    }.bind(this), this.props.searchDelay);
  },

  onFocus: function() {
    /** Called when the input/search field is focused **/
    if (this.blurTimer) {
      clearTimeout(this.blurTimer);
      this.blurTimer = null;
    }
    this.refs.search.getDOMNode().focus();
  },

  onBlur: function() {
    /** Called when the input/search field loses focus **/

    // wrap in setTimeout so we can catch a click on results
    this.blurTimer = setTimeout(function(){
      if (this.isMounted()) {
        this.setState({showResults: false});
      }
    }.bind(this), 100);
  },

  onQueryKeyDown: function(e) {
    /** Handles navigating the suggestions with they keyboard **/

    var getFocusedValueIndex = function() {
      // helper function to find the focussed value index when using the keyboard navigation
      if (!this.state.focusedValue) { return -1; }
      for (var i = 0, len = this.state.results.length; i < len; i++) {
        if (this.state.results[i].id === this.state.focusedValue.id)
          return i;
      }
      return -1;
    }.bind(this);
    if (this.state.showResults && e.key === 'Escape') {
      e.preventDefault();
      this.setState({ showResults: false });
    }
    else if (this.state.showResults && e.key === 'Enter') {
      e.preventDefault();
      if (this.state.focusedValue) {
        this.onValueChange(this.state.focusedValue);
      }
    } else if (this.state.showResults && e.key === 'ArrowUp') {
      e.preventDefault();
      var prevIdx = Math.max(getFocusedValueIndex() - 1, 0);
      this.setState({focusedValue: this.state.results[prevIdx]});

    } else if (e.key === 'ArrowDown') {
      e.preventDefault();
      if (this.state.showResults) {
        var nextIdx = Math.min(
          getFocusedValueIndex() + (this.state.showResults ? 1 : 0),
          this.state.results.length - 1
        );
        this.setState({ focusedValue: this.state.results[nextIdx]});
      } else if(this.state.searchTerm) {
        this.setState({ showResults: true });
      } else {
        this.showAllResults();
      }
    }
  }
});

var Result = React.createClass({

  render: function() {
    var classes = ['result'];

    if(this.props.focused)
      classes.push('active');

    var className = classes.join(' ');

    return (
      <li style={{listStyleType: 'none'}}>
        <a href="javascript:void(0)" className={className} onClick={this.onClick} onMouseEnter={this.onMouseEnter}>
          {this.props.result.title}
        </a>
      </li>
    );
  },

  onClick: function() {
    /** Relays the click action to the onClick prop **/
    this.props.onClick(this.props.result);
  },

  onMouseEnter: function(e) {
    /** Relays the hover effects to the onMouseEnter prop (if provided) **/
    if (this.props.onMouseEnter) {
      this.props.onMouseEnter(e, this.props.result);
    }
  },

  shouldComponentUpdate: function(nextProps) {
    /** Short-circuits the updates if not needed **/
    return (nextProps.result.id !== this.props.result.id ||
            nextProps.focused !== this.props.focused);
  }
});

var Results = React.createClass({

  render: function() {

    var style = {
      display: 'block',
      position: 'absolute'
    };

    return (
      <ul style={style} className={this.props.className} onFocus={this.props.onFocus} onSelect={this.props.onSelect}>
      { this.props.results.length ?
        this.props.results.map(this.renderResult)
        : this.props.showResultsInProgress ? <li><a href="javascript:void(0)" className="result">Loading...</a></li>
          : <li className="no-results"><a href="javascript:void(0)" className="result">{this.props.noResults}</a></li>
        }
      </ul>
    );
  },

  renderResult: function(result) {
    /** a helper function the render method **/
    var focused = this.props.focusedValue &&
                  this.props.focusedValue.id === result.id;
    return (
      <this.props.renderer
        ref={focused ? "focused" : undefined}
        key={result.id + result.title}
        result={result}
        focused={focused}
        onMouseEnter={this.onMouseEnterResult}
        onClick={this.props.onSelect}/>
    );
  },

  getDefaultProps: function() {
    /** Sets the default renderer if one has not been provided. **/
    return {renderer: Result};
  },

  componentDidUpdate: function() {
    this.scrollToFocused();
  },

  componentDidMount: function() {
    this.scrollToFocused();
  },

  componentWillMount: function() {
    this.ignoreFocus = false;
  },

  scrollToFocused: function() {
    /** Scrolls to the focussed result item **/
    var focused = this.refs && this.refs.focused;
    if (focused) {
      var containerNode = this.getDOMNode();
      var scroll = containerNode.scrollTop;
      var height = containerNode.offsetHeight;

      var node = focused.getDOMNode();
      var top = node.offsetTop;
      var bottom = top + node.offsetHeight;

      // we update ignoreFocus to true if we change the scroll position so
      // the mouseover event triggered because of that won't have an
      // effect
      if (top < scroll) {
        this.ignoreFocus = true;
        containerNode.scrollTop = top;
      } else if (bottom - scroll > height) {
        this.ignoreFocus = true;
        containerNode.scrollTop = bottom - height;
      }
    }
  },

  onMouseEnterResult: function(e, result) {
    // check if we need to prevent the next onFocus event because it was
    // probably caused by a mouseover due to scroll position change
    if (this.ignoreFocus) {
      this.ignoreFocus = false;
    } else {
      // we need to make sure focused node is visible
      // for some reason mouse events fire on visible nodes due to
      // box-shadow
      var containerNode = this.getDOMNode();
      var scroll = containerNode.scrollTop;
      var height = containerNode.offsetHeight;

      var node = e.target;
      var top = node.offsetTop;
      var bottom = top + node.offsetHeight;

      if (bottom > scroll && top < scroll + height) {
        this.props.onFocus(result);
      }
    }
  }
});