var Pager = React.createClass({
  /** React component for paging items, implements the bootstrap pager style.
   * === PROPS ===
   * totalCount: The number of items in the full set
   * pageLength: The number of items per page
   * currentPage: The number of the current page loaded
   * pageAction: The function called which will change pages.  The function should take a 'pageNumber' parameter.
   **/
  propTypes: {
    totalCount: React.PropTypes.number.isRequired,
    pageLength: React.PropTypes.number.isRequired,
    currentPage: React.PropTypes.number.isRequired,
    pageAction: React.PropTypes.func.isRequired
  },
  pageAction: function(page){
    return this.props.pageAction.bind(null, page);
  },
  shouldComponentUpdate: function(nextProps, nextState){
    // though rendering is very fast ~ 0.5 milliseconds, it's 2 orders of magnitude quicker to check here first.
    for(var key in this.props){
      if(this.props[key] !== nextProps[key]) {
        return true; // re-render given any difference
      }
    }
    return false;
  },
  render: function(){
    var currentPage = this.props.currentPage;
    var pageLength = this.props.pageLength;

    var totalCount = this.props.totalCount;

    var lowCount = (currentPage-1) * pageLength + 1;
    var highCount = Math.min(lowCount + pageLength-1, totalCount);

    var totalPages = Math.ceil(totalCount / pageLength);
    
    var prevDisabled = (currentPage == 1) ? ' disabled' : '';

    var nextDisabled = (currentPage == totalPages) ? ' disabled' : '';

    var innerButtons = [];

    // get a count of number of buttons before and after current, by stepping out from middle
    var preBtnCount = 0, postBtnCount = 0, distance = 1;
    while(preBtnCount + postBtnCount < 6){ // allow 7 inner buttons
      var added = false;
      if(currentPage - distance > 0){
        preBtnCount++;
        added = true;
      }
      if(currentPage + distance <= totalPages){
        postBtnCount++;
        added = true;
      }
      if(!added) break;
      distance++;
    }

    var start = currentPage-preBtnCount;
    var end = currentPage+postBtnCount;

    // Add buttons to array
    for(var i=start; i<=end; i++){
      var key = innerButtons.length;
      switch(i){
        // Case for active button should be dominant
        case currentPage:
          innerButtons.push(<li className="active" key={key}><a href="javascript:void(0)">{currentPage}</a></li>);
          break;

        // Case for the first element
        case start:
          innerButtons.push(<li key={key} onClick={this.pageAction(1)}><a href="javascript:void(0)">1</a></li>);
          break;

        // Case for last element
        case end:
            innerButtons.push(<li key={key} onClick={this.pageAction(totalPages)}><a href="javascript:void(0)">{totalPages}</a></li>);
            break;

        // Case for second inner-button from end
        case start+1:
        case end-1:
          if(i==2 || i==totalPages-1)
            innerButtons.push(<li key={key} onClick={this.pageAction(i)}><a href="javascript:void(0)">{i}</a></li>);
          else
            innerButtons.push(<li key={key} className="disabled"><a href="javascript:void(0)">...</a></li>);
          break;

        // All others
        default:
          innerButtons.push(<li key={key} onClick={this.pageAction(i)}><a href="javascript:void(0)">{i}</a></li>);
      }
    }

    var infoText;
    if(pageLength > 0) {
      infoText = <div className="page-counts dataTables_info hidden-xs" id="event-table_info" role="status" aria-live="polite">
          Showing <span className="txt-color-darken">{lowCount} </span> to <span className="txt-color-darken">{highCount} </span>
          of <span className="text-primary">{totalCount}</span>
          </div>;
    }
    else {
      infoText = <div className="page-counts dataTables_info hidden-xs" id="event-table_info" role="status" aria-live="polite">
          Showing <span className="txt-color-darken"> 0 </span>
          of <span className="text-primary">{totalCount}</span>
          </div>
    }
    return (
      <div className="row pager-row">
        <div className="col-sm-6">
        {infoText}
        </div>
        <div className="col-sm-6">
          <ul className="pagination">
            <li className={'prev' + prevDisabled} onClick={prevDisabled ? null : this.pageAction(currentPage-1)}>
              <a href="javascript:void(0)">
                <i className="fa fa-caret-left" />
              </a>
            </li>
            {innerButtons}
            <li className={'next' + nextDisabled} onClick={nextDisabled ? null : this.pageAction(currentPage+1)}>
              <a href="javascript:void(0)">
                <i className="fa fa-caret-right" />
              </a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
});