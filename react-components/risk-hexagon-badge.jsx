var RiskHexagonBadge = React.createClass({
  /** React component for a risk badge that is colored and in the shape of a hexagon.
   * === REQUIREMENTS ===
   * This component requires the utilities/utilities.js script
   * === PROPS ===
   * label: the character to place on the badge
   * percentage: the percentage value used for color (0 - 100), 0 being red, 100 being green.
   **/
  propTypes: {
    label: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ]),
    percentage: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ])
  },

  render: function(){
    return (
      <div className="dnb-badge-stack">
        <div className="dnb-badge" style={{color: percentageToColorVal(this.props.percentage)}}></div>
        <div className="dnb-badge-label">{this.props.label}</div>
      </div>
    );
  }
});