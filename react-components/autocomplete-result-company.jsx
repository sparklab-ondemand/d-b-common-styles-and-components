var CompanyResult = React.createClass({
  /** React component a company result for the autocomplete component.
   * === PROPS ===
   * result: the result object which will be displayed, expected to have the following properties: title, type, territory, country
   *         Ex: {title: 'Company Name', type: 'Headquarters', territory: 'Texas', country: 'US',}
   **/
  propTypes: {
    result: React.PropTypes.object
  },

  render: function() {
    var classes = ['result', 'company-result'];

    if(this.props.focused)
      classes.push('active');

    var className = classes.join(' ');

    return (
      <li style={{listStyleType: 'none'}}><a href="javascript:void(0)" className={className}
        onMouseEnter={this.onMouseEnter} onClick={this.onClick}>
        <div>
          <strong>{this.props.result.title}</strong>
          { this.props.result.type ?
            <small> - {this.props.result.type}</small>
            : null
          }
        </div>
        <div>
          {this.props.result.territory}
          {this.props.result.territory && this.props.result.country ? ', ' : null}
          {this.props.result.country}
        </div>
      </a></li>
    );
  },

  onClick: function() {
    /** Relays the click action to the onClick prop **/
    this.props.onClick(this.props.result);
  },

  onMouseEnter: function(e) {
    /** Relays the hover effects to the onMouseEnter prop (if provided) **/
    if (this.props.onMouseEnter) {
      this.props.onMouseEnter(e, this.props.result);
    }
  },

  shouldComponentUpdate: function(nextProps) {
    /** Short-circuits the updates if not needed **/
    return (nextProps.result.duns !== this.props.result.duns ||
            nextProps.focused !== this.props.focused);
  }
});